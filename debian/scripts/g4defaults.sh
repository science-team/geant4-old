# Default flags used for compiling user programs (these are sort of like
# Gentoo Linux "use flags").

# Source /etc/default/geant4, then $HOME/.geant4-defaults to determine
# what use flags the user wants.  We do this inside a subshell and echo
# the desired results to eval to avoid polluting the current environment.

# Since things can only be turned on, not off, there is no conflict if
# the user sets any of these variables manually.

eval $(
    (
	[ -e /etc/default/geant4 ]      && . /etc/default/geant4      || true
	[ -e "$HOME/.geant4-defaults" ] && . "$HOME/.geant4-defaults" || true
	
	# Test whether we want AIDA
	if [ ! "${USE_AIDA#[Yy1]}" = "$USE_AIDA" ] ; then
		# Test if AIDA is installed
		if [ -n "`which aida-config 2> /dev/null`" ] ; then
			echo 'export G4ANALYSIS_USE=1'
			echo 'export G4ANALYSIS_AIDA_CONFIG_CFLAGS="`aida-config --cflags`"'
			echo 'export G4ANALYSIS_AIDA_CONFIG_LIBS="`aida-config --libs`"'
		fi	
	fi

	# G4VIS_BUILD_OIX_DRIVER unset since OpenInventor is not in Debian
	# if [ ! "${USE_OIX#[Yy1]}" = "$USE_OIX" ] ; then
	#	echo 'export G4VIS_BUILD_OIX_DRIVER=1'
	#	echo 'export G4VIS_USE_OIX=1'
	# fi

	# Command prompt interface
	if [ ! "${USE_TCSH#[Yy1]}" = "$USE_TCSH" ] ; then
		echo 'export G4UI_USE_TCSH=1'
	fi

	# Xaw
	if [ ! "${USE_XAW#[Yy1]}" = "$USE_XAW" ] ; then
		echo 'export G4UI_BUILD_XAW_SESSION=1'
		echo 'export G4UI_USE_XAW=1'
	fi

	# Lesstif
	if [ ! "${USE_LESSTIF#[Yy1]}" = "$USE_LESSTIF" ] ; then
		echo 'export G4UI_BUILD_XM_SESSION=1'
		echo 'export G4UI_USE_XM=1'
	fi

	# DAWN
	if [ ! "${USE_DAWN#[Yy1]}" = "$USE_DAWN" ] ; then
		echo 'export G4VIS_BUILD_DAWN_DRIVER=1'
		echo 'export G4VIS_USE_DAWN=1'
	fi

	# Raytracer for X
	if [ ! "${USE_RAYTRACERX#[Yy1]}" = "$USE_RAYTRACERX" ] ; then
		echo 'export G4VIS_BUILD_RAYTRACERX_DRIVER=1'
		echo 'export G4VIS_USE_RAYTRACERX=1'
	fi

	# VRML
	if [ ! "${USE_VRML#[Yy1]}" = "$USE_VRML" ] ; then
		echo 'export G4VIS_BUILD_VRML_DRIVER=1'
		echo 'export G4VIS_USE_VRML=1'
	fi

	# G3toG4
	if [ ! "${USE_G3TOG4#[Yy1]}" = "$USE_G3TOG4" ] ; then
		echo 'export G4LIB_BUILD_G3TOG4=1'
		echo 'export G4LIB_USE_G3TOG4=1'
	fi

	# Zlib
	if [ ! "${USE_ZLIB#[Yy1]}" = "$USE_ZLIB" ] ; then
		echo 'export G4LIB_USE_ZLIB=1'
	fi

	# Compile with debugging info?
	if [ ! "${G4DEBUG#[Yy1]}" = "$G4DEBUG" ] ; then
		echo 'export G4DEBUG=1'
	fi

	# Compile "noisily" (i.e. output all compiler flags)?
	if [ ! "${CPPVERBOSE#[Yy1]}" = "$CPPVERBOSE" ] ; then
		echo 'export CPPVERBOSE=1'
	fi
	
	# Build static libs?
	if [ ! "${BUILD_STATIC#[Yy1]}" = "$BUILD_STATIC" ] ; then
		echo 'export G4LIB_BUILD_STATIC=1'
	fi

	# Build shared libs?
	if [ ! "${BUILD_SHARED#[Yy1]}" = "$BUILD_SHARED" ] ; then
		echo 'export G4LIB_BUILD_SHARED=1'
	fi
    )
)

