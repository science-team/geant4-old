Geant4 for Debian
------------------

Contents:

I.   Optional data files
II.  The issue of environment variables
     1.  Using the wrapper scripts
     2.  Obtaining needed environment variables automatically
     3.  Setting needed environment variables by hand
III. OpenGL, the "invisible detector" problem and other visualization issues


I) Optional data files

If you want, you may install five sets of data provided by the Geant4 authors.
These data sets are optional; they are also extremely large, and they do not
appear to have a clear license.  If disk space or DFSG-freeness are important
to you, I suggest you not install them.  Otherwise, you may use the
install-geant4-data script (as root) in order to install them.  If you want
all of them, this is as simple as

	install-geant4-data all

(Warning: 370 megabytes!  You should have a fast Internet connection.  Also,
be warned that the script does NOT check to see if you have enough disk space
free.)

Or if you only want a specific one, such as PhotonEvaporation data, you can run

	install-geant4-data PhotonEvaporation

By default the data files are installed under /usr/share/geant4/data but you
can put them somewhere else by setting the $G4DATA environment variable before
running the script.

The install-geant4-data script also has support for uninstalling data files
and removing old obsolete data files; see its man page for more information.
Be aware that ALL data files will be deleted automatically if you uninstall
the geant4-common Debian package.

As of version 4.8.1.p01-2 of the Debian package, old obsolete data files are no
longer deleted automatically on upgrade of the package.  However, the data
files are not upgraded automatically either, since I suspect automatic
downloads of very large files would be considered undesirable.  Therefore,
after each upgrade of the Geant4 packages, you should run (as root) these
commands at a convenient time for downloading potentially large amounts of
data:

	install-geant4-data all
	install-geant4-data --clean all

(replacing "all" with the name(s) of the data set(s) you use if you need only
a subset of them).  Running these commands is optional, but if you do not do
so, Geant4 will use old obsolete data sets instead of the most up-to-date
versions.

You can see the currently-installed versions of the data sets by running

	/usr/sbin/install-geant4-data --version all

and compare with the most up-to-date supported versions by running
/usr/sbin/install-geant4-data with no arguments.


II) The issue of environment variables

Development of programs using the Geant4 libraries, and running said programs,
frequently requires a large number of Geant4-specific environment variables to
be set.  This comes into conflict with Debian Policy, which states that a
program "must not depend on environment variables to get reasonable defaults."

To resolve the issue, I provide two good ways (and one not-so-good way) in
which users of the Geant4 Debian packages can work.  You may use the provided
(Debian-specific) wrapper scripts "g4make" and "g4run"; source a specific file
in your login scripts that provides all the needed environment variables; or
set them all yourself by hand.  In the first two cases, the only environment
variables you may want to set by hand (not mandatory) are $G4WORKDIR and
$G4ENV_VERBOSITY.  These three ways of working are described in detail below.


1) Using the wrapper scripts

If you think (as I do) that environment variables are at best a necessary evil,
you can usually avoid using them completely by using the g4make and g4run
wrapper scripts.  In this case, anywhere you would normally type "make" to
compile a Geant4 program, instead type "g4make".  Any time you would run a
Geant4 program, instead type "g4run" followed by the program name and
arguments (if any).  See the man pages for g4make and g4run for more
information.

Please note that every time you run one of these wrapper scripts, they will
temporarily use the present working directory $PWD for the value of $G4WORKDIR,
the Geant4 working directory.  In some cases, for instance if $PWD is not
writable or if you use several Geant4 programs that depend upon each other but
were compiled in different directories, this may cause problems.  In such
cases, you can first set the $G4WORKDIR environment variable yourself to the
desired place, either at the command prompt or in a shell login script.  For
instance:

	export G4WORKDIR="$HOME/geant4"	# for Bourne-compatible shells
	setenv G4WORKDIR "$HOME/geant4"	# for csh-compatible shells

(This directory is in fact what g4make / g4run will use by default if $PWD is
not writable.)

This and $G4ENV_VERBOSITY (see below) are the only environment variables you
may have any wish to set in this mode of working.  You may set the interfaces
and visualizations that will be compiled into your programs by editing the file
/etc/default/geant4 or by copying it to a file in your home directory named
".geant4-defaults" and editing that.

Take care that other Geant4-related environment variables such as $G4INSTALL
are NOT set!  If you have already set $G4INSTALL to something other than the
Debian-specific value of /usr/lib/geant4, the wrapper scripts will not know
what to do, so they may not function properly.


2) Obtaining needed environment variables automatically

If you don't mind having your environment namespace polluted by dozens of
Geant4-specific environment variables, you can set them automatically by
sourcing a setup script in your login files.  Simply put one of the following
sets of lines, depending upon your shell flavor, into one of your login files.
(Consult the documentation for your preferred shell to determine which file to
place it in.  Some common names for login files are .login, .bash_profile,
.cshrc, .tcshrc, and .zshrc.)

	# For Bourne-compatible shells (sh, dash, bash, ksh, zsh, etc.)
	if [ -e /usr/share/geant4/env.sh ] ; then
		. /usr/share/geant4/env.sh
	fi

	# For csh-compatible shells (csh, tcsh)
	if ( -e /usr/share/geant4/env.csh ) then
		source /usr/share/geant4/env.csh
	endif

To determine the automatically set values of the Geant4 environment variables,
run the command "g4env" after starting a new shell that has sourced the above
login commands.

You now have no need to use the "g4make" or "g4run" wrapper scripts (although
doing so does no harm).  In this mode of working, you may want to set the
$G4WORKDIR variable yourself before this point in your login file.  Otherwise
it will default to $HOME/geant4, and that directory will automatically be
created if it does not already exist.

You can also set the level of verbosity of the setup script:

	export G4ENV_VERBOSITY=low	# for Bourne-compatible shells
	setenv G4ENV_VERBOSITY low	# for csh-compatible shells

Possible values are "silent", "low", "medium", and "high".  If this variable is
unset or set to an unrecognized value, it defaults to "high".  (The default
when running g4make or g4run is "medium.")

If you like, you may set the interfaces and visualizations that will be
compiled into your programs by editing the file /etc/default/geant4 or by
copying it to a file in your home directory named ".geant4-defaults" and
editing that.  Or, you may do it manually with statements in your login files
like "export G4UI_USE_XM=1 G4UI_BUILD_XM_SESSION=1", etc.

Take care that other Geant4-related environment variables such as $G4INSTALL
are NOT set elsewhere in your login files!  If you have already set $G4INSTALL
to something other than the Debian-specific value of /usr/lib/geant4, the setup
script will not know what to do, so it will not set any other Geant4
environment variables.


3) Setting needed environment variables by hand

I do not recommend this, but if you really want to fine-tune your environment,
have a system modified in an unusual way, or use a weird shell that is based on
neither the Bourne shell nor csh, then you may want to set the Geant4
environment variables by hand in your login files.  Running the command

	g4run g4env

at the shell prompt will give you a starting point by telling you what the
Debian-specific default values are.  Be sure that all Geant4-related
environment variables are unset before doing so.

Please note that as you install, remove or upgrade different Geant4-related
Debian packages, some of the Debian-specific default values may change, and
you will probably want to mirror the changes in your manually set values.


III) OpenGL, the "invisible detector" problem and other visualization issues

First of all, in order to have OpenGL interfaces even be available, you will
need to install the "libg4opengl-dev" Debian package before building any Geant4
programs that want to use OpenGL.  (This should be installed automatically if
you installed the "geant4" metapackage that depends upon everything
Geant4-related.)  That said, the OpenGL visualization interfaces used by Geant4
are not the most stable pieces of software.

Various problems I and others have experienced with them include, in order of
increasing severity, the "invisible detector" problem, failure of certain
interfaces even to load (crashing the Geant4 program in question), and even a
crash of the X server!

The invisible detector problem, in which particle trajectories are drawn but
the detector volume is not, appears to be related to issues with video card
drivers.  It seems that in general, when open source video drivers succumb to
this issue, proprietary drivers (ATI's fglrx or NVIDIA's driver) do not.  More
information, which may or may not be helpful, is available at this thread on
the Geant4 HyperNews forum:

http://geant4-hn.slac.stanford.edu:5090/HyperNews/public/get/visualization/366/7.html

With the NVIDIA legacy drivers in Debian Etch (package "nvidia-glx-legacy"), I
have observed program crashes when trying to use the OGLIXm (OpenGLImmediateXm)
visualization driver, with errors like:

X Error of failed request:  BadMatch (invalid parameter attributes)
  Major opcode of failed request:  1 (X_CreateWindow)
  Serial number of failed request:  855
  Current serial number in output stream:  912

N.B. my video card is an nVidia NV5 [RIVA TNT2/TNT2 Pro].

With the free Xorg "nv" driver (package "xserver-xorg-video-nv"), in addition
to the invisible detector problem while using OGLIX, I have observed program
crashes when trying to use the OGLIXm and OGLSXm visualization interfaces.
My attempts to use the OGLSX interface with the free "nv" driver have resulted
in crashes of the X server!

 -- Kevin B. McCarty <kmccarty@debian.org>, Wed, 12 Jun 2008
