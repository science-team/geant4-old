# env.sh: Set up environment variables for running Geant4.

# This file is located at /usr/share/geant4/env.sh and may be sourced
# from your .bashrc, .zshrc, or other Bourne-shell-compatible shell
# initialization files like this:
#	. /usr/share/geant4/env.sh

# Note that you may want to first set $G4WORKDIR yourself; if not set,
# it will be assumed to be $HOME/geant4 if you source this script from a
# login script.  That directory will be created if necessary.  This should
# be OK, though.  You can change it at any time by setting it manually
# (nothing in this script depends upon the location of $G4WORKDIR).

# Note that the behavior of the g4make and g4run wrapper scripts will be
# different if you have not explicitly set $G4WORKDIR; they will each use
# the current directory at the time they are called.  If you have explicitly
# set $G4WORKDIR, they will respect your setting.

# You can control the verbosity of this script by setting the environment
# variable $G4ENV_VERBOSITY to one of the values {silent, low, medium, high}.
# If unset or set to something else, it defaults to high.

case "$G4ENV_VERBOSITY" in
	silent|low|medium|high)	: ;;
	*)
		if [ -n "$PS1" ] ; then
			if [ -n "$g4non_display" ] ; then
				G4ENV_VERBOSITY=medium
			else
				G4ENV_VERBOSITY=high
			fi
		else
			# If we don't have an interactive terminal and
			# $G4ENV_VERBOSITY is not already set, turn off messages
			G4ENV_VERBOSITY=silent
		fi
		export G4ENV_VERBOSITY
		;;
esac

# Check whether $G4INSTALL is already set.  If it is set to something
# other than the value expected for the Debian packages, bail out.

if [ -n "$G4INSTALL" ] && [ ! "$G4INSTALL" = /usr/lib/geant4 ] ; then
	# The user has set $G4INSTALL to something other than the
	# Debian packaging value.  Trust that they know what they're doing...
	if [ ! "$G4ENV_VERBOSITY" = low ] && [ ! "$G4ENV_VERBOSITY" = silent ]
	then
		echo \
'You have set the environment variable $G4INSTALL to something other than
the value /usr/lib/geant4 used in the Debian packages of Geant4.
I trust you know what you are doing (probably you have installed another
version of Geant4 by hand?).  Please ensure you have also set all other
needed Geant4 environment variables appropriately.'
		echo
		echo \
'You can turn off this message by setting the environment variable
$G4ENV_VERBOSITY to "low", or by not sourcing /usr/share/geant4/env.sh
or env.csh in your login scripts (as you seem not to need it) and not
using the "g4make" and "g4run" wrapper scripts.'
		echo
	fi

# If $G4INSTALL is not set, set up the Geant4 Debian-specific environment.
else
	# Run all the fragments in the Geant4 sh script directory.
	for script in /usr/share/geant4/sh-scripts.d/*.sh ; do
		. "$script"
	done
fi

# If the user asks for it by setting $G4ENV_VERBOSITY=high,
# print the values of all the Geant4 environment variables.
# (The printenv.sh script requires bash to work as it uses variable
# indirection.)
if [ "$G4ENV_VERBOSITY" = high ] && which bash > /dev/null 2>&1 ; then
	bash /usr/share/geant4/printenv.sh
fi

