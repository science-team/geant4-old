# Default flags used for compiling user programs (these are sort of like
# Gentoo Linux "use flags").

# Source /etc/default/geant4, then $HOME/.geant4-defaults to determine
# what use flags the user wants.  We do this inside a subshell and echo
# the desired results to eval to avoid polluting the current environment.

# Since things can only be turned on, not off, there is no conflict if
# the user sets any of these variables manually.

eval `(

	[ -e /etc/default/geant4 ]      && . /etc/default/geant4      || true
	[ -e "$HOME/.geant4-defaults" ] && . "$HOME/.geant4-defaults" || true

	# OpenGL driver
	if [ ! "${USE_OPENGL#[Yy1]}" = "$USE_OPENGL" ] ; then
		echo 'export G4VIS_BUILD_OPENGLX_DRIVER=1'
		echo 'export G4VIS_USE_OPENGLX=1'
	fi

	# OpenGL / Lesstif driver
	if [ ! "${USE_OPENGL_LESSTIF#[Yy1]}" = "$USE_OPENGL_LESSTIF" ] ; then
		echo 'export G4VIS_BUILD_OPENGLXM_DRIVER=1'
		echo 'export G4VIS_USE_OPENGLXM=1'
	fi
)`

# We also apparently have to set this, for some reason:
[ -n "$OGLHOME" ] || export OGLHOME=/usr

