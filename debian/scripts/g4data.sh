# Set the location of the Geant4 data

[ -n "$G4DATA" ] && [ -d "$G4DATA" ] || G4DATA=/usr/share/geant4/data
export G4DATA

# Test for the existence of individual data sets

if [ -z "$G4LEDATA" ] ; then
	G4LEDATA="$(/usr/sbin/install-geant4-data --version G4EMLOW)"
	[ -z "$G4LEDATA" ] || G4LEDATA="$G4DATA/$G4LEDATA"
fi

# As of Geant4 8.2, G4ELASTICDATA is no longer used
#if [ -z "$G4ELASTICDATA" ] ; then
#	G4ELASTICDATA="$(/usr/sbin/install-geant4-data --version G4ELASTIC)"
#	[ -z "$G4ELASTICDATA" ] || G4ELASTICDATA="$G4DATA/$G4ELASTICDATA"
#fi

if [ -z "$G4LEVELGAMMADATA" ] ; then
	G4LEVELGAMMADATA="$(/usr/sbin/install-geant4-data \
		--version PhotonEvaporation)"
	[ -z "$G4LEVELGAMMADATA" ] || \
		G4LEVELGAMMADATA="$G4DATA/$G4LEVELGAMMADATA"
fi

if [ -z "$G4RADIOACTIVEDATA" ] ; then
	G4RADIOACTIVEDATA="$(/usr/sbin/install-geant4-data \
		--version G4RadioactiveDecay)"
	[ -z "$G4RADIOACTIVEDATA" ] || \
		G4RADIOACTIVEDATA="$G4DATA/$G4RADIOACTIVEDATA"
fi

if [ -z "$G4NEUTRONHPDATA" ] ; then
	G4NEUTRONHPDATA="$(/usr/sbin/install-geant4-data \
		--version G4NDL)"
	[ -z "$G4NEUTRONHPDATA" ] || \
		G4NEUTRONHPDATA="$G4DATA/$G4NEUTRONHPDATA"
fi

if [ -z "$G4ABLADATA" ] ; then
	G4ABLADATA="$(/usr/sbin/install-geant4-data \
		--version G4ABLA)"
	[ -z "$G4ABLADATA" ] || G4ABLADATA="$G4DATA/$G4ABLADATA"
fi


# Check to make sure the data exist (at least, the directories)
[ -d "$G4LEDATA"         ] && export G4LEDATA         || unset G4LEDATA
#[ -d "$G4ELASTICDATA"   ] && export G4ELASTICDATA    || unset G4ELASTICDATA
[ -d "$G4LEVELGAMMADATA" ] && export G4LEVELGAMMADATA || unset G4LEVELGAMMADATA
[ -d "$G4RADIOACTIVEDATA" ] && \
		export G4RADIOACTIVEDATA || unset G4RADIOACTIVEDATA
[ -d "$G4NEUTRONHPDATA" ] && \
		export G4NEUTRONHPDATA || unset G4NEUTRONHPDATA
[ -d "$G4ABLADATA"       ] && export G4ABLADATA       || unset G4ABLADATA

