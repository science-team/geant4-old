# env.csh: Set up environment variables for running Geant4.

# This file is located at /usr/share/geant4/env.csh and may be sourced
# from your .cshrc or .tcshrc shell initialization files like this:
#	source /usr/share/geant4/env.csh

# Note that you may want to first set $G4WORKDIR yourself; if not set,
# it will be assumed to be $HOME/geant4 if you source this script from a
# login script.  That directory will be created if necessary.  This should
# be OK, though.  You can change it at any time by setting it manually
# (nothing in this script depends upon the location of $G4WORKDIR).

# Note that the behavior of the g4make and g4run wrapper scripts will be
# different if you have not explicitly set $G4WORKDIR; they will each use
# the current directory at the time they are called.  If you have explicitly
# set $G4WORKDIR, they will respect your setting.

# You can control the verbosity of this script by setting the environment
# variable $G4ENV_VERBOSITY to one of the values {silent, low, medium, high}.
# If unset or set to something else, it defaults to high.


# The following is an ugly hack, but it's better than maintaining
# two sets of script fragments (one set for each shell type).

# First, output the results of running env.sh to the terminal while
# saving the list of environment variables (found under the same sh shell)
# into a temporary file.  (Would prefer to use a shell variable, but csh
# complains "Word too long" :-( )  Note that if $prompt is defined, we
# tell sh it is running interactively so env.sh knows it can print output.

set g4_sh_env="`/bin/tempfile -s .sh`"
if ( $?prompt == 1 ) then
	sh -ci ". /usr/share/geant4/env.sh ; export > '$g4_sh_env'"
else
	sh -c ". /usr/share/geant4/env.sh ; export > '$g4_sh_env'"
endif

# Now massage the list of exported variables into a csh script and run it.
# The second grep -E filters out lines of 255 or more characters, to avoid
# breaking csh's fragile little mind.

set g4_csh_env="`/bin/tempfile -s .csh`"
grep -E '^export ([gG]4|CLHEP|(OGL|OIV|XM|XAW)(HOME|FLAGS|LIBS)|CPPVERBOSE)' \
	"$g4_sh_env" | grep -E -v '^.{255,}$' | \
	sed 's/^export \([^=]*\)=/setenv \1 /g' > "$g4_csh_env"
source "$g4_csh_env"

# Clean up.

rm -f "$g4_csh_env" "$g4_sh_env"
unset g4_sh_env
unset g4_csh_env

