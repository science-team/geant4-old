define(`BINARY_EQUIV', `$1 (= ${binary:Version})')dnl
dnl
define(`SOURCE_EQUIV', `$1 (= ${source:Version})')dnl
dnl
define(`UPSTREAM_VERSION', `$1 (>= $2), $1 (<< $2.0~)')dnl
dnl
define(`UPSTREAM_EQUIV', `UPSTREAM_VERSION(`$1', ${source:Upstream-Version})')dnl
dnl
define(`GEANT_SONAME', `$1-'GEANT4_RELEASE`-'GEANT4_SOVER)dnl
dnl
define(`GEANT_LIB', GEANT_SONAME($1)`'GEANT4_ABI)dnl
dnl
define(`GEANT_ABI_CONFLICTS',
	ifelse(GEANT4_ABI, `', `',
`Conflicts: 'GEANT_SONAME($1)`
Replaces: 'GEANT_SONAME($1)`
'))dnl
dnl	
define(`GEANT_DEPS', `libxmu-dev, libxt-dev, libx11-dev, lesstif2-dev, libxaw7-dev, zlib1g-dev, UPSTREAM_VERSION(clhep2-dev, CLHEP_VERSION)')dnl
dnl
define(`OPENGL_DEPS', `libglu1-mesa-dev | libglu-dev, libgl1-mesa-dev | libgl-dev, libxi-dev')dnl
dnl
define(`GEANT_HOME', `http://geant4.cern.ch/')dnl
dnl
define(`GEANT_SHORT',
`Geant4 is a toolkit for the simulation of the passage of particles through
 matter.  Its application areas include high energy and nuclear physics.')dnl
dnl
define(`GEANT_DESC',
`Geant4 is a toolkit for the simulation of the passage of particles through
 matter.   Its application areas include high energy physics and nuclear
 experiments, medical, accelerator and space physics studies.  The Geant4
 software consists of an extensive set of libraries, fundamentally
 object-oriented and based in modern C++.  It may be used to develop programs
 with command-line, Xaw-based, or Motif-based interfaces.')dnl
dnl
