# Set the location of the CLHEP library.

[ -n "$CLHEP_LIB" ] || export CLHEP_LIB=CLHEP
if [ -z "$CLHEP_BASE_DIR" ] ; then
	if [ -f /usr/lib/lib${CLHEP_LIB}.so ] ; then
		export CLHEP_BASE_DIR=/usr
	elif [ -f /usr/local/lib/lib${CLHEP_LIB}.so ] ; then
		export CLHEP_BASE_DIR=/usr/local
	elif [ ! "$G4ENV_VERBOSITY" = silent ] ; then
		echo \
'Warning: Cannot find the CLHEP library!  You will have to set the
environment variables $CLHEP, $CLHEP_BASE_DIR, $CLHEP_INCLUDE_DIR,
and $CLHEP_LIB_DIR yourself.' 2>&1
		echo 2>&1
	fi
fi

if [ -n "$CLHEP_BASE_DIR" ] ; then
	[ -n "$CLHEP_INCLUDE_DIR" ] || \
		export CLHEP_INCLUDE_DIR="$CLHEP_BASE_DIR/include"
	[ -n "$CLHEP_LIB_DIR" ] || \
		export CLHEP_LIB_DIR="$CLHEP_BASE_DIR/lib"
fi

