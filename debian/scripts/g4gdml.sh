# Set the location of the xerces libraries, and turn on GDML if found.

for dir in "$XERCESCROOT" /usr /usr/local ; do
	XERCESCROOT=""
	if [ -f "$dir"/lib/libxerces-depdom.so ] && \
	   [ -f "$dir"/lib/libxerces-c.so ] ; then
		export XERCESCROOT="$dir"
		break
	fi
done

if [ -n "$XERCESCROOT" ] ; then
	export G4LIB_BUILD_GDML=1
elif [ ! "$G4ENV_VERBOSITY" = silent ] ; then
	echo \
'Warning: Cannot find both of libxerces-c.so and libxerces-depdom.so in the
same directory!  You will have to set the environment variable $XERCESCROOT
yourself.' 2>&1
	echo 2>&1
fi
