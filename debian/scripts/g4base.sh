# Set the expected Geant4 env. variables to Debian-specific values

export G4INSTALL=/usr/lib/geant4
[ -n "$G4SYSTEM"  ] || export G4SYSTEM=Linux-g++
[ -n "$G4LIB"     ] || export G4LIB="$G4INSTALL"
[ -n "$G4LIBDIR"  ] || export G4LIBDIR="$G4LIB"

