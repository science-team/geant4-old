# Make sure $G4WORKDIR is set

if [ -z "$G4WORKDIR" ] ; then
	if [ ! "$G4ENV_VERBOSITY" = low ] && [ ! "$G4ENV_VERBOSITY" = silent ]
	then
		echo \
'Since the environment variable $G4WORKDIR must be set, I will set it to
$HOME/geant4 and create that directory for you if possible.  If you would
prefer a different fallback working directory, please set it before running
g4make or g4run or sourcing /usr/share/geant4/env.sh or env.csh in your shell
initialization scripts.'
		echo
		echo \
'You can also turn off this message by setting the environment variable
$G4ENV_VERBOSITY to "low".'
		echo
	fi
	G4WORKDIR="$HOME/geant4"
fi

# Make sure $G4WORKDIR exists

if [ ! -e "$G4WORKDIR" ] && [ ! -L "$G4WORKDIR" ] ; then
	if [ ! "$G4ENV_VERBOSITY" = silent ] ; then
		echo "Trying to create \$G4WORKDIR=\"$G4WORKDIR\" ..."
	fi

	if ! mkdir "$G4WORKDIR" > /dev/null 2>&1 ; then
		if [ ! "$G4ENV_VERBOSITY" = silent ] ; then
			echo 'Failed!  Falling back to G4WORKDIR=$HOME' 2>&1
			echo
		fi
		G4WORKDIR="$HOME"
	else
		if [ ! "$G4ENV_VERBOSITY" = silent ] ; then
			echo 'Success!'
			echo
		fi
	fi

elif [ ! -d "$G4WORKDIR" ] || [ ! -w "$G4WORKDIR" ] ; then
	if [ ! "$G4ENV_VERBOSITY" = silent ] ; then
		echo \
'Warning: The location '"$G4WORKDIR"' is not a directory or not
writable!  Falling back to G4WORKDIR=$HOME' 2>&1
	fi
	G4WORKDIR="$HOME"
fi

export G4WORKDIR

# Now define $G4BIN, $G4BINDIR

if [ -z "$G4BINDIR" ] ; then
	[ -n "$G4BIN" ] || G4BIN="$G4WORKDIR/bin"
	G4BINDIR="$G4BIN/$G4SYSTEM"
	if mkdir -p "$G4BINDIR" ; then
		export G4BIN G4BINDIR
	else
		if [ ! "$G4ENV_VERBOSITY" = silent ] ; then
			echo \
'Warning: The directory for holding newly created Geant4 binaries,
'"$G4BINDIR"', cannot be created!  Please fix this.' 2>&1
			echo 2>&1
		fi
	fi
fi

